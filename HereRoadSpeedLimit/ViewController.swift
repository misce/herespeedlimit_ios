//
//  ViewController.swift
//  HereRoadSpeedLimit
//
//  Created by Curiologix on 02/03/2018.
//  Copyright © 2018 Ali Raza. All rights reserved.
//

import UIKit
import CoreLocation
import BugfenderSDK

struct Static{
    static let AppID = "rXX5IKsqFcjkAAmzOhjp"
    static let AppCode = "uXJlJau0wP2fAWv6MvnsNQ"
    static let Radius = "25"
    static let LicenseCode = "PYsDnmQTPrayzjpSrfdKRulwCKF9JsAFpIBKBi+bRBlJmcxzrw0yMdk/7chaEJTfmQ2wj0Pgfd68DIT5FyObRTdLMsZHbEgBckTC2LtYOrO2w/WOGZS1xer+2ESD4Ejir2AKPAuNXUIxFpi2mfxm79ZrZrk3/oEkMEkcTDUQ0W3KlBXor4TPGdeFXQIqw9l3Eegwl45y9rjX4YNHNvP/AQaNjVvUUa6Xad516NEnJVlQrFHzz5/HpX9metuEAEnkhezgv4/eOe+kYotqxs0rjmDgrOnSSrxpZ/ZIfDIEeDghqC7l1GXk9bRAj42r4MJWVtHcKlm6k9Ho3eJmGcpr/rFNwBrsrd/wjvQPJzuEYRKipOCc0nE7UQ4fecmzbalS/AMG/37qTYX73NT/F8PJDyb4wiGLB3f3krUFer5cGyDVB535tKo19f/giLeA4h/7hX2lFBKw/fj1kH+Zb3KZGG34AmkVS4cMawW5WQRxVRrpsXRiXR5jCSUXazyBwddcJraYThIAHe3RlWx4Z7+ZNp8mERjjmq+lSOsUSXBUM66VRy0Tvls2gH9tAYDY24eAyLujkNNVT9rLtQQt8rHgZtKKxpQzzjMWqSvkhyA3R/G8XV3uQkuPH8Ge6D6fGHKIO9fBUWlADBazP3dYqwbsjC0cc6NSnl82oZjCNg7jODU="
}

class ViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet var indicator : UIActivityIndicatorView!
    @IBOutlet var lbl_streetMaxSpeedKM : UILabel!
    @IBOutlet var lbl_streetMaxSpeedMph : UILabel!
    
    let locationManager = CLLocationManager()
    var findSpeedLimit = false;
    var locationCount = 0;
    var hearCount = 0;
    override func viewDidLoad() {
        super.viewDidLoad()

        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy =
                kCLLocationAccuracyHundredMeters
            self.locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.lbl_streetMaxSpeedKM.layer.cornerRadius =  self.lbl_streetMaxSpeedKM.frame.size.width / 2;
        self.lbl_streetMaxSpeedMph.layer.cornerRadius =  self.lbl_streetMaxSpeedMph.frame.size.width / 2;
        
        self.lbl_streetMaxSpeedKM.layer.borderWidth = 10.0;
        self.lbl_streetMaxSpeedMph.layer.borderWidth = 10.0;
        
        self.lbl_streetMaxSpeedKM.layer.borderColor = UIColor.red.cgColor;
        self.lbl_streetMaxSpeedMph.layer.borderColor = UIColor.red.cgColor;
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        BFLog("%%%%%hereCount/locationCount=\(self.hearCount)/\(self.locationCount)%%%%%%")
        BFLog("=============locationCount=\(self.locationCount)======")
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.locationCount = self.locationCount+1;
        BFLog("locations update received = \(locValue.latitude) \(locValue.longitude)")
        if !self.findSpeedLimit
        {
            self.indicator.startAnimating();
            self.findSpeedLimit = true;
            
            self.callFirstHereAPi(coordinate: (manager.location?.coordinate)!);
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                self.findSpeedLimit = false;
            })
            
        }
    }
    
    // call first api to get geo coder
    func callFirstHereAPi(coordinate:CLLocationCoordinate2D)
    {
        let url : String =  "https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json?prox=\(coordinate.latitude),\(coordinate.longitude),\(Static.Radius)&mode=retrieveAddresses&locationAttributes=linkInfo&gen=9&app_id=\(Static.AppID)&app_code=\(Static.AppCode)"
        
        BFLog("---First APi Call = "+url);

        let task = URLSession.shared.dataTask(with: NSURL(string: url)! as URL) { (data, response, error) -> Void in

            BFLog(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String)
            if let data = data {
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    {
                        let referenceID =  self.getReferenceIdAndFunctionClass(dicResponse: jsonResult, info: "MapReference", id: "ReferenceId");
                        let functionClass =  self.getReferenceIdAndFunctionClass(dicResponse: jsonResult, info: "LinkInfo", id: "FunctionalClass");
                        
                        if (referenceID.characters.count > 0 && functionClass.characters.count > 0)
                        {
                            self.callSecondLevelHereAPi(refID: referenceID, functionClass: Int(functionClass)!, coordinate: coordinate);
                        } else
                        {
                            DispatchQueue.main.async {
                                self.lbl_streetMaxSpeedKM.text = "0";
                                self.lbl_streetMaxSpeedMph.text = "0";
                                self.indicator.stopAnimating()
                                /// here is comment
                            }
                        }
                    }
                } catch let error as NSError {
                    BFLog(error.localizedDescription)
                }
            } else if let error = error {
                BFLog(error.localizedDescription)
            }
        }
        task.resume()
    }
    
    // call first api to get geo coder
    func callSecondLevelHereAPi(refID:String, functionClass:Int,coordinate:CLLocationCoordinate2D)
    {
        let url : String =  "https://pde.api.here.com/1/index.json?app_id=\(Static.AppID)&app_code=\(Static.AppCode)&layer=ROAD_GEOM_FCn&attributes=LINK_ID&values=\(refID)"
        BFLog("---Second APi Call = "+url);
        let task = URLSession.shared.dataTask(with: NSURL(string: url)! as URL) { (data, response, error) -> Void in
            
            BFLog(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String)
            if let data = data {
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    {
                        if let layer: NSArray = jsonResult["Layers"] as? NSArray
                        {
                            for layerDic in layer
                            {
                                if let levelDic: NSDictionary = layerDic as? NSDictionary
                                {
                                    let level : String = String(describing: (levelDic["level"] as? NSNumber)!);
                                    if (level.characters.count > 0)
                                    {
                                        self.calculateTilesWithRefIDAndFunctionClass(refID: refID, functionClass: Int(functionClass), coordinate: coordinate,slevel: level);
                                    }
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    BFLog(error.localizedDescription)
                }
            } else if let error = error {
                BFLog(error.localizedDescription)
            }
        }
        task.resume()
    }
    //PDE 3rd call
    func calculateTilesWithRefIDAndFunctionClass(refID:String, functionClass:Int,coordinate:CLLocationCoordinate2D,slevel:String)
    {
        let level = 8 + functionClass;
        let tileSize = 180 / pow(2, level);
        let tileY = Int(((coordinate.latitude + 90) / Double(truncating: tileSize as NSNumber)));
        let tileX = Int(((coordinate.longitude + 180) / Double(truncating: tileSize as NSNumber)));
        
        let url : String = "https://pde.api.here.com/1/tile.json?layer=SPEED_LIMITS_FC\(functionClass)&tilex=\(tileX)&tiley=\(tileY)&level=\(slevel)&app_id=\(Static.AppID)&app_code=\(Static.AppCode)"
        BFLog("---Third APi Call = "+url);
        
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL) { data, response, error in
            BFLog(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as String)
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                {
                    let speedLimit = self.findSpeedLimit(jsonResult: jsonResult, referenceID: refID);
                    if (speedLimit.characters.count > 0)
                    {// we got valid speed limit here
                        BFLog("^^^^^hereCount=\(self.hearCount)^^^^^") ;                   self.hearCount = self.hearCount+1;
                        DispatchQueue.main.async {
                            self.lbl_streetMaxSpeedKM.text = speedLimit;
                            let speedMPh = Double(speedLimit)! * 0.621371;
                            self.lbl_streetMaxSpeedMph.text = String(Int(speedMPh));
                            self.indicator.stopAnimating();
                        }
                    }
                }
            } catch let error as NSError {
                BFLog(error.localizedDescription)
                self.indicator.stopAnimating();
            }
            
            }.resume()
    }
    
    func findSpeedLimit(jsonResult:NSDictionary,referenceID:String) -> String
    {
        if let rows: NSArray = jsonResult["Rows"] as? NSArray
        {
            for row in rows
            {
                if let limitDic: NSDictionary = row as? NSDictionary
                {
                    if let linkID : String = limitDic["LINK_ID"] as? String
                    {
                        if linkID == referenceID
                        {
                            if let limit : String = limitDic["FROM_REF_SPEED_LIMIT"] as? String
                            {
                                return limit;
                            } else  if let limit : String = limitDic["TO_REF_SPEED_LIMIT"] as? String
                            {
                                return limit;
                            }
                        }
                    }
                }
            }
        }
        return "";
    }
    
    func getReferenceIdAndFunctionClass(dicResponse:NSDictionary, info:String, id:String)  -> String
    {
        if let jsonResult : NSDictionary = dicResponse["Response"] as? NSDictionary
        {
            if let results: NSArray = jsonResult["View"] as? NSArray
            {
                for hereView in results
                {
                    if let hView: NSDictionary = hereView as? NSDictionary
                    {
                        if let result: NSArray = hView["Result"] as? NSArray
                        {
                            for hereView in result
                            {
                                if let hView: NSDictionary = hereView as? NSDictionary
                                {
                                    if let location: NSDictionary = hView["Location"] as? NSDictionary
                                    {
                                        if let mapRef: NSDictionary = location[info] as? NSDictionary
                                        {
                                            if let refID: String = mapRef[id] as? String
                                            {
                                                return refID;
                                            } else if let refID: NSNumber = mapRef[id] as? NSNumber
                                            {
                                                return String(describing: refID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return "";
    }
}

